<?php
declare(strict_types=1);

require_once('vendor/autoload.php');

use WSKZ\Controllers\BaseController;

$controller = new BaseController();
echo $controller->getController();
