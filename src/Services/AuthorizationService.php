<?php

namespace WSKZ\Services;

class AuthorizationService
{
    private const LOGIN = 'admin';
    private const PASSWORD = 'admin';

    public function authorize()
    {
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        if ((empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW'])) || $_SERVER['PHP_AUTH_USER'] != self::LOGIN
            || $_SERVER['PHP_AUTH_PW'] != self::PASSWORD) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }
    }
}