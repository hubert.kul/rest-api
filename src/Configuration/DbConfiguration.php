<?php
declare(strict_types=1);

namespace WSKZ\Configuration;

class DbConfiguration
{
    private string $hostname;
    private string $user;
    private string $password;
    private string $db;
    private string $charset;

    public function __construct()
    {
        $this->user = 'hubert';
        $this->password = 'oHubert1!';
        $this->hostname = 'localhost';
        $this->db = 'wskz';
        $this->charset = 'utf8mb4';
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getDb(): string
    {
        return $this->db;
    }

    /**
     * @return string
     */
    public function getHostname(): string
    {
        return $this->hostname;
    }

    /**
     * @return string
     */
    public function getCharset(): string
    {
        return $this->charset;
    }

    public function getDSN(): string
    {
        return "mysql:host=" . $this->hostname . ';dbname=' . $this->db . ';charset=' . $this->charset;
    }
}
