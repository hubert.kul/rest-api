<?php
declare(strict_types=1);

namespace WSKZ\Controllers;

use WSKZ\Services\ApiResponse;
use WSKZ\Services\NumberService;

class NumberController
{
    public function __call($name, $arguments)
    {
        return (new ApiResponse('', 404, 'Method dont exist'))->getResponse();
    }

    public function process(string $method, int $param)
    {
        return $this->{$method}($param);
    }

    public function generate(): array
    {
        $insertId = (new NumberService())->save();
        return (new ApiResponse($insertId))->getResponse();
    }

    public function retrieve(int $id): array
    {
        $number = (new NumberService())->find($id);

        if ($number === '') {
            return (new ApiResponse('', 404, 'Wrong ID'))->getResponse();
        }

        return (new ApiResponse($number))->getResponse();
    }
}