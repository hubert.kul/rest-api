<?php

namespace WSKZ\Utils;

interface ResponseStatusInterface
{
    public const status = [
        200 => 'OK',
        404 => 'Not Found',
        500 => 'Internal Server Error',
    ];
}