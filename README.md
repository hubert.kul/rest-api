1. UTWÓRZ TABELE:

CREATE TABLE numbers (
id INT auto_increment NOT NULL,
value VARCHAR(32) NOT NULL,
CONSTRAINT numbers_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4;

2. Wykonaj `composer install`
3. Skonfiguruj połączenie w pliku src/Configuration/DbConfiguration.php
4. Uruchom aplikacje z katalogu głownego np. jako alias rest-api.pl
5. Wywołania API:
   number/generate - generuje liczbę, zapisuje ją do bazy i zwraca ID
   number/retrieve/{id} - zwraca wczesniej wylosowaną liczbę na podstawie ID

Dla przykładowej domeny:
http://rest-api.pl/number/generate
http://rest-api.pl/number/retrieve/{id}
