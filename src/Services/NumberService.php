<?php
declare(strict_types=1);

namespace WSKZ\Services;

use PDO;
use WSKZ\Configuration\DbConfiguration;

class NumberService
{
    public const MAX_NUMBER = 100000;
    private PDO $pdo;

    public function __construct()
    {
        $dbConfiguration = new DbConfiguration();
        $this->pdo = new PDO($dbConfiguration->getDSN(), $dbConfiguration->getUser(), $dbConfiguration->getPassword());
    }

    public function generateNumber(): int
    {
        return rand(1, self::MAX_NUMBER);
    }

    public function save(): string
    {
        $stmt = $this->pdo->prepare('INSERT INTO numbers (value) VALUES (?)');
        $stmt->execute([$this->generateNumber()]);
        return $this->pdo->lastInsertId();
    }

    public function find(int $id): string
    {
        $stmt = $this->pdo->prepare('SELECT value FROM numbers WHERE id = ?');
        $stmt->bindValue(1, $id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row['value'] ?? '';
    }
}