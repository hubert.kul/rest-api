<?php

namespace WSKZ\Controllers;

use WSKZ\Services\AuthorizationService;
use WSKZ\Utils\ResponseStatusInterface;

class BaseController
{
    private string $method;
    private string $controller;
    private string $param;

    public function __construct()
    {
        $authorization = new AuthorizationService();
        $authorization->authorize();
        $this->getRoute();
    }

    public function getController()
    {
        switch ($this->controller) {
            case 'number':
                $api = new NumberController();
                $response = $api->process($this->method, $this->param);
                return $this->response($response, $response['code']);

            default:
                return $this->response('', 404);
        }
    }

    private function getRoute()
    {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $uri = explode('/', $uri);

        if (empty($uri[1]) || empty($uri[2])) {
            $this->response('', 404);
        }

        $this->controller = $uri[1];
        $this->method = $uri[2];
        $this->param = (!empty($uri[3]) ? (int)$uri[3] : 0);
    }

    private function response($data, $status = 200)
    {
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: GET");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
        header("HTTP/1.1 " . $status . " " . $this->statuses($status));
        return json_encode(['response' => $data]);
    }

    private function statuses(int $code): string
    {
        return ResponseStatusInterface::status[$code] ?? ResponseStatusInterface::status[500];
    }
}
