<?php

namespace WSKZ\Services;

class ApiResponse
{
    private int $code;
    private mixed $data;
    private string $msg;

    public function __construct(mixed $data, int $code = 200, string $msg = '')
    {
        $this->code = $code;
        $this->data = $data;
        $this->msg = $msg;
    }

    public function getResponse(): array
    {
        return [
            'code' => $this->code,
            'data' => $this->data,
            'msg' => $this->msg,
        ];
    }
}